\documentclass{report}
\usepackage[numbers,square,sort&compress]{natbib}
\usepackage{hyperref}

\title{Randomized Testing of the WebAssembly System Interface}

\author{Ethan Stanley}

\date{\today}

\usepackage{graphicx}

\renewcommand\thesection{\arabic{section}}

\begin{document}
\maketitle

\begin{abstract}
	The WebAssembly System Interface (WASI) enables WebAssembly (Wasm) programs to interact with the facilities of the computer on which the Wasm program runs. This greatly expands Wasm’s utility and power outside of the browser, but greater capability increases the risk of vulnerabilities that arise when Wasm runtimes are not implemented correctly. This is especially important given Wasm’s emphasis on security. It is therefore necessary to thoroughly test implementations of WASI. We propose to evaluate the effectiveness of randomized testing for finding bugs in implementations of WASI. We propose to create a system to perform differential testing on Wasm runtimes. This system will randomly generate Rust test cases that invoke system calls and compile them to x86 assembly and Wasm. If the runtime behaviors of these executables differ, we expect that there is a bug in one of the systems under test (SUT). We will evaluate the effectiveness of our randomized testing framework based on the number of unique WASI defects found, the percentage of calls in the WASI API covered, and the number of test cases evaluated.
\end{abstract}

\tableofcontents
\pagebreak

\section{Introduction}

The goal of this study is to evaluate the effectiveness of randomized testing for identifying bugs in WASI-compliant runtimes. 
Specifically, we propose to use differential testing to identify randomly generated test cases that trigger bugs. 

WebAssembly, or Wasm, is a portable compilation target that has been widely adopted in web browsers \cite{wasm-spec}. Recently, interest in using Wasm outside of the browser led to the creation of WASI. WASI is a system interface for Wasm that enables interaction with the operating system. It serves a similar purpose as C's standard library.

Wasm prioritizes security by employing a memory safe, sandboxed execution environment \cite{wasm-home}. WASI attempts to uphold Wasm's security guarantees by sandboxing I/O operations. Prior to execution, a Wasm program must be given explicit permission to use a feature of WASI. For example, WASI programs can only modify directories that are specified from the command line.

An incorrect implementation of WASI could undermine the security of Wasm. In addition, WASI greatly increases the capability of Wasm programs, so the consequences of vulnerabilities caused by bugs in WASI can be more severe. It is therefore necessary to thoroughly and effectively test WASI-compliant runtimes. At the time of writing, no efforts to apply randomized testing to WASI have been documented.

We propose to do this by creating a differential testing framework for WASI. Testing WASI is a distinct goal from testing Wasm. To test Wasm, one would generate Wasm programs that maximize coverage of Wasm's syntax and semantics. In contrast, effectively testing WASI involves generating interesting and varied calls to the WASI API. This does not necessarily require all of the features of Wasm.

Differential testing is a software testing technique that attempts to detect bugs by providing the same input to different implementations of a software system and comparing their behaviors \cite{mckeeman}. If the specification of the systems allows only a single possible outcome, and the behaviors of the systems differ, then one of them must be inconsistent with its specification. Differential testing offers a solution to the test oracle problem: the challenge of distinguishing correct and incorrect behavior in software \cite{oracle}. With differential testing, any test case can be fed to a software system as long as it does not contain unspecified or non-deterministic behavior. This combines powerfully with a random test case generator, for which determining correct behavior would otherwise be difficult.

In this study, we propose to compare the behaviors of a Wasm program and an x86 binary. To get executables we expect to behave equivalently, we take advantage of the fact that they are both compilation targets of Rust. Thus, given a Rust test case that invokes system calls, a pair of executables can be generated that is suitable for performing differential testing on WASI. 

In the current version of WASI \cite{wasi-preview-1}, API calls are provided for generating random numbers and interacting with files, clocks, and sockets. Our test case generator will produce programs that maximize coverage of these features.

We hypothesize that randomized testing is an effective method for testing implementations of WASI. We will evaluate this claim with several metrics. Our primary metric is the number of unique defects we find in WASI-compliant runtimes. If defects are discovered, we can conclude that our hypothesis is correct. Secondary metrics include the percentage of WASI API calls our test case generator can produce and the total number of test cases we evaluate. These metrics will increase our confidence in the conclusions we make about the effectiveness of our test case generator, whether or not we are successful in finding bugs.

WASI, like WebAssembly, is an evolving standard. Our differential testing framework can be used as a development tool for Wasm runtimes as WASI becomes more widely implemented. Currently, there are only two implementations of WASI: the Bytecode Alliance’s Wasmtime \cite{wasmtime} and a browser shim \cite{shim}. However, this number will certainly increase.


\section{Related Work}

Differential testing is widely used to find bugs in software systems. Differential testing can be applied to compilers using test cases from random program generators. Csmith is a well-known example \cite{csmith}.

Creating a random program generator can be a complex process. For instance, the development of Csmith required hundreds of
hours and nearly 40,000 lines of code \cite{xsmith}. There is particular difficulty in generating programs that are suitable as test cases for differential testing. These programs must avoid unspecified and non-deterministic behavior. Otherwise, one cannot be confident that differences in behavior between systems under test are due to the existence of a bug.

Recently, Xsmith was introduced to make the development of random program generators, especially those intended for differential testing, simpler \cite{xsmith}. Xsmith is a domain-specific language built within Racket \cite{plt-tr1} for defining random program generators. It allows the user to specify a grammar that is used to randomly construct an abstract syntax tree (AST) representing a program. Xsmith also allows the user to define choice methods and attributes to arbitrarily guide the generation of the AST. Xsmith contains predefined properties to make adding common programming language features easier.

Xsmith has been used to implement several random program generators that have been used to identify bugs in compilers \cite{xdsmith, wasmlike}. One of these program generators is wasmlike \cite{wasmlike}, a generator of random Wasm programs. Wasmlike is an inspiration for this project, and one method of testing WASI implementations would be to extend wasmlike to make use of the WASI API. This would result in the direct generation of WASI test cases, rather than obtaining them by compiling Rust test cases. However, we would not be able to perform differential testing with this approach because only one reliable implementation of WASI currently exists. Thus, we need to compile Rust programs to WASI and x86 assembly in order to have enough targets for differential testing. Otherwise, solving the test oracle problem becomes much more difficult.

The technique of compiling to x86 and Wasm from a high level language was previously used to evaluate the security risks of porting C programs to Wasm \cite{porting-c-to-wasm}. 
The authors evaluated the equivalence of the binaries by comparing their return codes and standard output. 
We use these same comparisons, but we extend them to include the effects on the file system as well as data written over sockets.

A major component of this study is developing a random program generator to provide Rust test cases. 
Several Rust program generators already exist \cite{rustsmith}, \cite{dewey}. 
Sharma et al. introduce RustSmith, a Rust language fuzzer for differential testing implemented with Kotlin. 
RustSmith was used to find historical and previously unreported bugs in various Rust compilers. 
Dewey et al. created a Rust test case generator to demonstrate how constraint logic programming can be used to generate well-typed programs \cite{dewey}. 
With this tool, they test the Rust typechecker by generating programs that either adhere to or subtly violate Rust’s type rules. 
Our test case generator will differ from both of these program generators because it will prioritize generation of interesting sequences of system calls over stressing a compiler’s adherence to Rust's semantics.

To our knowledge, no prior attempts to perform randomized testing on implementations of WASI have been documented. 
However, they have been tested in other ways. 
Users of Wasmtime regularly submit bug reports related to WASI \cite{wasi-bugs}.
These reports were recently categorized by Zhang et~al.\ when they developed a taxonomy of bugs discovered in Wasm implementations \cite{wasm-bug-taxonomy}. 
They created a testing framework by collecting known bug-revealing test cases into a test suite. 
This was effective at finding bugs in untested Wasm implementations. 
However, it is still beneficial to have a framework for generating an arbitrary number of test cases, rather than having to update a test suite by hand.
Additionally, a randomized testing framework can reveal bugs before they are encountered by users.

\section{Proposed Work}
We propose to build a Rust test case generator and a harness for differential testing. We will use these components to run testing campaigns with the goal of finding bugs in the WASI subsystem of Wasmtime, the WebAssembly runtime created by the Bytecode Alliance. Figure~\ref{fig:pipeline} illustrates our proposed differential testing pipeline.

\begin{figure}
	\centering
	\includegraphics{flowchart.pdf}
	\caption{Proposed differential testing pipeline}
	\label{fig:pipeline}
\end{figure}

\subsection{Methodology}

To obtain Rust test cases, we will build a test case generator called Wasimilar. 
We will create it using Xsmith.
Wasimilar will generate Rust programs that contain random sequences of operations that can be compiled to WASI.
The primary data structure in the test cases will be buffers that can be used to perform operations with files and sockets. Results from system calls without visible effects, such as requests for random numbers or the current time, are incorporated into buffers so their results can be made visible. Buffers serve as an interface through which system calls can affect one another.

We will create a testing harness that automates the process of differential testing. The harness will be driven by two scripts: (1) a driver to generate, compile, and run a test case, and (2) a predicate to determine if the generated binaries behaved equivalently. The driver script will run each target in an empty directory and keep a log of the program's standard output. A network server will be used to record all data sent to it by the targets using WASI's socket API. The predicate script will then compare the directories, logged standard output, and data received by the server to determine if the scripts behaved equivalently.

With a test case generator and testing harness in place, we can run fuzzing campaigns to find bugs in the WASI subsystem of Wasmtime. These testing campaigns will be carried out on CloudLab \cite{cloudlab}, a cloud computing testbed for research. This will allow us to run extended campaigns using multiple machines in order to evaluate a large number of test cases.

If we succeed in finding test cases that exhibit different behavior when compiled to x86 assembly and Wasm, further work will be needed to determine if a bug has truly been found. We will use an existing Rust program reducer \cite{c-reduce} to simplify the test case while preserving the bug-triggering behavior. If the test case is deterministic and well-defined, we can conclude that a bug has been found and include the simplified test case in a bug report.

\subsection{Evaluation}

A metric of success for this project is the number of unique defects found in the WASI subsystem of Wasmtime. 
If we are able to find defects and have them confirmed by the developers of Wasmtime, we will conclude that randomized testing is an effective method for finding bugs in WASI implementations.

Another metric we will use is the percentage of the WASI API covered by our generated test cases. If we are able to generate every API call with our test cases, we can be more confident that we are thoroughly testing WASI.

A third metric is the number of test cases we evaluate. If we generate a large number of test cases, we get a more accurate assessment of the effectiveness of our approach, whether or not any bugs were found.

We may use a code coverage tool to assess our coverage of a system under test, but WASI is only a fraction of the functionality that Wasm runtimes implement. It is therefore unclear what a desirable amount of code coverage would be.

\subsection{Completed Work}

We currently have a preliminary version of the test case generator and a simple harness implemented. The generator produces test cases that perform file reads, file writes, and random number requests. Below is a simplifed example of a test case generated by Wasimilar. It demonstrates seeding random number generation, opening a file, creating a random buffer, and writing the buffer to a file.

\begin{verbatim}
fn main() -> std::io::Result<()>{
    let mut r = StdRng::seed_from_u64(753790);
    let mut var_1 = OpenOptions::new().create(true)
                                      .append(true)
                                      .open("var_1")?;
    let mut lift_2 = String::from("xllfmkeflobhpydm");
    var_1.write_all(lift_2.as_bytes())?;
    Ok(())
}
\end{verbatim}

The preliminary test harness implements the testing pipeline shown in Figure~\ref{fig:pipeline}. We are using small testing campaigns on CloudLab to guide development of the harness and test case generator.

\section{Proposed Schedule}

\begin{tabular}{p{1in} p{3.5in}}
	11/7 – 12/15&Finalize implementation of the test case generator and incorporate it into a testing harness. 
	Run fuzzing campaigns on CloudLab to iron out problems with the test case generator and harness.\\
	12/16 - 1/7&Run fuzzing campaigns to search for bugs in WASI subsystem of Wasmtime. Reduce, verify, and report any bugs found.\\
	1/8 – 3/20&Work on thesis document. Continue running fuzzing campaigns.\\
	3/21 – 3/31&Prepare and practice for the formal oral presentation.\\
	3/31&Complete draft of thesis document.\\
	4/1&Do the formal oral presentation.\\
	4/2 – 4/8&Prepare to present at the Undergraduate Research Symposium (URS).\\
	4/9&Present at the URS.\\
	4/10&Seek signatures for advisor-approved thesis.\\
	4/20&Submit finalized thesis.\\
\end{tabular}

\bibliographystyle{ACM-Reference-Format}
\bibliography{papers}

\end{document}
